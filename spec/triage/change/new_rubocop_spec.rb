# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/triage/change/new_rubocop'

RSpec.describe Triage::NewRuboCop do
  describe '.file_patterns' do
    it 'matches rubocop files' do
      files = %w[
        .rubocop.yml
        .rubocop.yaml
        .rubocop_todo.yml
        .rubocop_todo.yaml
        .rubocop_todo/layout/hash_alignment.yml
        .rubocop_todo/class_and_module_children.yml
      ]

      expect(files).to all(match(described_class.file_patterns))
    end
  end

  describe '.line_patterns' do
    it 'matches enabling cops' do
      diffs = [
        '+ Enabled: true',
        '+ Enabled:true',
        '- Enabled: false',
        '- Enabled:false'
      ]

      expect(diffs).to all(match(described_class.line_patterns))
    end
  end

  describe '#type' do
    it 'returns the class name without namespace' do
      expect(described_class.new({}).type).to eq('NewRuboCop')
    end
  end
end
