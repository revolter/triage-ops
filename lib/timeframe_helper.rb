# frozen_string_literal: true

# A helper for any resource that observes a start_date and due_date. For
# example, milestones or epics.
module TimeframeHelper
  def days_until_due_date(timeframeable)
    (timeframeable.due_date - Date.today)
  end

  def days_since_start_date(timeframeable)
    (Date.today - timeframeable.start_date)
  end
end
