# frozen_string_literal: true

require_relative '../change'

module Triage
  class ESLintChange < Change
    def self.file_patterns
      # rubocop:disable Style/RegexpLiteral
      @file_patterns ||=
        %r{ ^\.eslintrc\.yml$
          }x
      # rubocop:enable Style/RegexpLiteral
    end
  end
end
