# frozen_string_literal: true

require 'slack-messenger'
require_relative '../change/eslint_change'

module Triage
  class PingChangesToJiHuSlack
    INTERESTED_CHANGES = [
      ESLintChange
    ].freeze

    SLACK_CHANNEL = 'frontend-change-watchers'
    SLACK_USERNAME = 'JiHu notifier'
    SLACK_ICON = ':robot_face:'

    attr_reader :processor

    def initialize(new_processor)
      @processor = new_processor
    end

    def applicable?
      slack_webhook_url &&
        processor.event.merge_event? &&
        changes_should_notify.any?
    end

    def react
      slack_messenger.ping(text: message)
    end

    private

    def message
      "Changes for #{interested_changed_paths} detected in #{processor.event.url}"
    end

    def slack_webhook_url
      ENV['JH_SLACK_WEBHOOK_URL']
    end

    def interested_changed_paths
      @interested_changed_paths ||= changes_should_notify.map do |change|
        "`#{change.path}`"
      end.join(', ')
    end

    def changes_should_notify
      @changes_should_notify ||= INTERESTED_CHANGES.flat_map do |change|
        change.detect(processor.changed_file_list)
      end
    end

    def slack_messenger
      @slack_messenger ||= Slack::Messenger.new(
        slack_webhook_url,
        channel: SLACK_CHANNEL,
        username: SLACK_USERNAME,
        icon_emoji: SLACK_ICON)
    end
  end
end
