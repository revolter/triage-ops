# frozen_string_literal: true

require_relative '../change/new_rubocop'

module Triage
  class CommentChangesForJiHu
    INTERESTED_CHANGES = [
      NewRuboCop
    ].freeze

    attr_reader :processor

    def initialize(new_processor)
      @processor = new_processor
    end

    def applicable?
      changes_should_notify.any? &&
        # Always notify for merge event, which should happen only once
        (processor.event.merge_event? || unique_comment.no_previous_comment?)
    end

    def react
      message =
        if processor.event.merge_event?
          notification_when_merged
        else
          notification_upon_detection
        end

      if discussion_thread
        processor.append_discussion(message, discussion_thread['id'], append_source_link: true)
      else
        processor.add_discussion(message, append_source_link: true)
        processor.resolve_discussion(discussion_thread['id'])
      end
    end

    # Avoid undercoverage error. This is not used yet
    # def documentation
    #   <<~MARKDOWN
    #     Notify JiHu about changes that might break JiHu
    #   MARKDOWN
    # end

    private

    def notification_when_merged
      notification do
        <<~MARKDOWN
          This merge request is merged with the following changes that might break JiHu:

          #{file_list_to_notify}
        MARKDOWN
      end
    end

    def notification_upon_detection
      notification do
        <<~MARKDOWN
          Changes detected for the following files that might break JiHu:

          #{file_list_to_notify}
        MARKDOWN
      end
    end

    def notification
      unique_comment.wrap(yield.strip)
    end

    def file_list_to_notify
      changes_should_notify.map do |change|
        "- `#{change.path}` (#{change.type})"
      end.join("\n")
    end

    def discussion_thread
      unique_comment.previous_discussion
    end

    def changes_should_notify
      @changes_should_notify ||= INTERESTED_CHANGES.flat_map do |change|
        change.detect(processor.changed_file_list)
      end
    end

    def unique_comment
      # This is using Triage::NotifyChanges for backward compatibility
      @unique_comment ||= UniqueComment.new('Triage::NotifyChanges', processor.event)
    end
  end
end
