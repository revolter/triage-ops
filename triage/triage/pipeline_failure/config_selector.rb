# frozen_string_literal: true

module Triage
  module PipelineFailure
    module ConfigSelector
      def self.find_config(event, configuration_classes)
        config = configuration_classes.find do |config_klass|
          config_klass.match?(event)
        end

        return unless config

        config.new(event)
      end
    end
  end
end
