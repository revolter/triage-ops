# frozen_string_literal: true

require_relative '../../triage/processor'
require_relative '../../../lib/constants/labels'

module Triage
  module Workflow
    class ExpediteLabels < Processor
      react_to 'merge_request.open', 'merge_request.update'

      def applicable?
        return false if (master_broken_label_set? || quarantine_label_set?) && pipeline_expedite_label_set?

        event.from_gitlab_org? &&
          [
            master_broken_label_set? || quarantine_label_set?,
            pipeline_expedite_label_set?
          ].any?
      end

      def process
        if master_broken_label_set? || quarantine_label_set?
          add_missing_label
        else
          remove_extra_label
        end
      end

      def documentation
        <<~TEXT
          1. Adds the `pipeline:expedite` label` to MRs that have the `master:broken`, `master:foss-broken`, or `quarantine` label
          2. Removes `pipeline:expedite` from MRs that don't have the `master:broken`, `master:foss-broken`, or `quarantine` label
        TEXT
      end

      private

      def master_broken_label_set?
        event.label_names.include?(Labels::MASTER_BROKEN_LABEL) ||
          event.label_names.include?(Labels::MASTER_FOSS_BROKEN_LABEL)
      end

      def quarantine_label_set?
        event.label_names.include?(Labels::QUARANTINE_LABEL)
      end

      def pipeline_expedite_label_set?
        event.label_names.include?(Labels::PIPELINE_EXPEDITE_LABEL)
      end

      def add_missing_label
        add_comment(%(/label ~"#{Labels::PIPELINE_EXPEDITE_LABEL}"), append_source_link: false)
      end

      def remove_extra_label
        comment = <<~MARKDOWN.chomp
          Setting ~"#{Labels::PIPELINE_EXPEDITE_LABEL}" without ~"#{Labels::MASTER_BROKEN_LABEL}", ~"#{Labels::MASTER_FOSS_BROKEN_LABEL}" or ~"#{Labels::QUARANTINE_LABEL}" is forbidden!
          /unlabel ~"#{Labels::PIPELINE_EXPEDITE_LABEL}"
        MARKDOWN

        add_comment(comment, append_source_link: false)
      end
    end
  end
end
